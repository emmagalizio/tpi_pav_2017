
create database MegDatabase;
go
use MegDatabase;
go

create table tipoDocumento( 
idTipoDocumento int not null primary key ,
tipo varchar(10) not null unique,
descripcion text not null

)
go
create table tipoPc(  
idTipoPc int not null primary key ,
tipo varchar(20) not null unique,
descripcion text not null


)
go
create table tipoMemoria( 
idTipoMemoria int not null primary key ,
tipo varchar(20) not null unique,
descripcion text not null

)
go
create table marcaProcesador( 
idMarca int not null primary key ,
marca varchar(15) not null unique

)
go
create table estadoOrdenTrabajo( 
idEstado int not null primary key ,
estado varchar(20) not null unique,
descripcion text not null
)

go
create table cobro( 
idCobro int not null primary key ,
fechaCobro date not null,
monto decimal(8,2) not null

)
go
create table tipoComponente( 
idTipoComponente int not null primary key ,
tipo varchar(60) not null unique,
descripcion text

)
go

create table proveedor( 
nroProveedor int not null primary key ,
razonSocial varchar(60) not null,
cuit varchar(11) unique ,
telefono varchar(20) ,
fechaBaja date 
)
go
create table marcaComponente( 
idMarcaComponente int not null primary key ,
marca varchar(45)

)
go

create table procesador( 
idProcesador int not null primary key ,
modeloProcesador varchar(60) not null unique,
frecuencia decimal(8,2) not null,
cantidadNucleos int not null,
marca int not null,
constraint FK_marcaProcesador foreign key(marca) references marcaProcesador(idMarca) 
ON UPDATE CASCADE

)
go
create table componente(
idComponente int not null primary key ,
descripcion text not null,
marca int not null,
tipoComp int not null,
numProveedor int,
garantia int not null default 0, 
precioCompra decimal(8,2) not null,
porcentajeGanancia int,
fechaCompra date not null,
constraint FK_marcaComponente foreign key(marca) references marcaComponente(idMarcaComponente) on update cascade,
constraint FK_tipoComponente foreign key(tipoComp) references tipoComponente(idTipoComponente) on update cascade,
constraint FK_proveedor foreign key(numProveedor) references proveedor(nroProveedor) on update cascade
)
go
create table servicio(  
idServicio int not null primary key ,
nombreServicio varchar(80) not null unique,
detalle text not null,
costo decimal(8,2) not null,
garantia int not null default 0,
fechaBaja date,
repuestoRequerido int, 
constraint FK_repuestoRequerido foreign key(repuestoRequerido) references tipoComponente(idTipoComponente)
)

go
create table cliente(  
idCliente int not null primary key ,
nombre varchar(20) not null,
apellido varchar(20) not null,
telefono varchar(20) not null,
fechaAlta date not null,
fechaBaja date,
email varchar(60),
nroDocumento varchar(11) not null,
tipoDoc int not null,
constraint FK_tipoDocumento foreign key(tipoDoc) references tipoDocumento(idTipoDocumento) ON DELETE NO ACTION ON UPDATE CASCADE,
constraint UQ_tipoNroDoc unique(nroDocumento,tipoDoc)

)
go

create table computadora(  
idComputadora int not null primary key ,
tipo int not null,
microProcesador int,
cantidadMemoria decimal(8,2),
tipoMem int,
almacenamiento int,
fechaAlta date not null,
fechaBaja date,
client int not null,
constraint FK_tipoPc foreign key(tipo)  references tipoPc(idTipoPc) ON UPDATE CASCADE,
constraint FK_procesador foreign key(microProcesador) references procesador(idProcesador) ON UPDATE CASCADE,
constraint FK_tipoMemoria foreign key(tipoMem) references tipoMemoria(idTipoMemoria) ON UPDATE CASCADE,
constraint FK_cliente foreign key(client) references cliente(idCliente)


)
go
create table ordenTrabajo( 
idOrdenTrabajo int not null primary key ,
fechaRecepcion date not null,
fechaReparacion date,
estado int not null,
compu int not null,
nroCliente int not null,
cobro int,
detalleFalla text not null,
monto decimal(8,2),
constraint FK_estadoOT foreign key(estado) references estadoOrdenTrabajo(idEstado) on update cascade,
constraint FK_computadora foreign key(compu) references computadora(idComputadora) on update cascade,
constraint FK_clienteOt foreign key(nroCliente) references cliente(idCliente) on update cascade, /*no la crea porque produce un ciclo de cascada*/
constraint FK_cobro foreign key(cobro) references cobro(idCobro) on update cascade


)
go
create table detalleOrdenTrabajo( 
idDetalleOrdenTrabajo int not null primary key ,
servicio int not null,
componente int,
cantidad int not null,
montoUnitarioServicio decimal(8,2) not null,
montoUnitarioComponente decimal(8,2),
idOrden int not null,
constraint FK_servicio foreign key(servicio) references servicio(idServicio) ON UPDATE CASCADE,
constraint FK_componenteDetalleOt foreign key(componente) references componente(idComponente) on update cascade,
constraint FK_ordenT foreign key(idOrden) references ordenTrabajo(idOrdenTrabajo) on update cascade
)
go


